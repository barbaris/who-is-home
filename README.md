# Who Is Home?

This Node.js application uses a Telegram bot to signal when someone comes home or leaves. The bot will send a message to a specified Telegram chat whenever the devices associated with the application connect or disconnect from the local Wi-Fi network.

## Requirements

To use this application, you will need the following:

- A device running Node.js
- A Telegram account and a bot token (see [Telegram Bot API documentation](https://core.telegram.org/bots/api))
- A Wi-Fi network with a permanent IP address for each device that needs to be tracked

Please make sure that all devices that need to be tracked have a permanent IP address on your Wi-Fi network. This is important because the script uses IP addresses to detect whether a device is connected to the network or not. If a device's IP address changes frequently, the script may not be able to accurately detect its presence on the network.

If you're not sure how to set a permanent IP address for a device on your Wi-Fi network, please consult the documentation for your Wi-Fi router or contact your Internet Service Provider (ISP) for assistance.

## Installation

1. Clone the repository to your device:

```
git clone https://gitlab.com/barbaris/who-is-home.git
```

2. Navigate to the cloned directory and install the required packages:

```
cd who-is-home.gi
npm install
```

## Configuration

1. Rename the example configuration `config.dist.json` file to `config.json`. Here's an example of what the configuration file might look like:

    ```
    {
      "pingInterval": 28000,
      "pingTimeout": 120000,
      "persons": [
        {
          "name": "User 1",
          "ip": "192.168.1.11"
        },
        {
          "name": "User 2",
          "ip": "192.168.1.12"
        }
      ]
    }
    ```
    
    The `pingInterval` and `pingTimeout` properties control the interval between ping requests and the timeout for a ping response, respectively. The `persons` array contains the list of persons to be monitored. Each person is represented as an object with a `name` and an `ip` property. The `name` property is a string that represents the name of the person, and the `ip` property is the IP address of the device that the person uses.
    
    You can modify the `config.json` file to change the configuration of the application as needed. Note that changes to the configuration file will require restarting the application for them to take effect.

2. Rename the `.env.example` file to `.env` and fill in the required configuration variables. The configuration variables are:
   - `TELEGRAM_BOT_TOKEN`: The token for your Telegram bot.
   - `TELEGRAM_CHAT_ID`: The ID of the Telegram chat where the bot will send messages.

3. Run the application:

    ```
    npm start
    ```

## Usage

Once the application is running, it will continuously monitor the network for connections to the monitored devices. When the device is connected, the bot will send a message to the specified Telegram chat with the text `<Username> welcome home!`. When the device disconnects, the bot will send a message with the text `<Username> left home N minutes ago.` (`N` is `pingTimeout` in minutes)

## License

This project is licensed under the [ISC License](https://opensource.org/license/isc-license-txt/). Feel free to use it for personal or commercial purposes.
