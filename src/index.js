require('dotenv').config();
const fs = require('fs');
const Person = require('./person');
const ping = require('ping');
const { Telegraf } = require('telegraf');
const persons = [];
const chats = [];
if (process.env.TELEGRAM_CHAT_ID) {
    chats.push(process.env.TELEGRAM_CHAT_ID);
}
const configStr = fs.readFileSync('config.json', 'utf8');
const config = JSON.parse(configStr);
for (const person of config.persons) {
    persons.push(new Person(person.name, person.ip));
}
const pingInterval = config.pingInterval;
const pingTimeout = config.pingTimeout;
const pingTimeoutInMinutes = pingTimeout / 1000 / 60;

const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN || (() => {
    throw Error('Telegram access token is not set!');
})());

bot.start((ctx) => {
    console.log('Telegram user has started bot', ctx.update.message.chat);
    if (chats.indexOf(ctx.update.message.chat.id) === -1) {
        chats.push(ctx.update.message.chat.id);
        ctx.reply('You are subscribed!');
    }
});

setInterval(() => {
    for (const person of persons) {
        ping.promise.probe(person.ip)
            .then(result => {
                if (person.ping === null) {
                    for (const chat of chats) {
                        bot.telegram.sendMessage(chat, 'Initial state for ' + person.name + ' is ' + (result.alive ? 'home' : 'not home'));
                    }
                    console.log('Initial state for ' + person.name + ' is ' + (result.alive ? 'home' : 'not home'));
                    person.ping = result.alive;
                    person.isHome = result.alive;
                    return;
                }
                if (person.ping === result.alive) {
                    return;
                }
                if (result.alive) {
                    if (person.timout) {
                        clearTimeout(person.timout);
                        person.timout = false;
                        console.log('Clearing timeout', person.timout);
                    }
                    if (!person.isHome) {
                        for (const chat of chats) {
                            bot.telegram.sendMessage(chat, person.name + ' welcome home!');
                        }
                        console.log(person.name + ' welcome home!');
                    }
                    person.isHome = true;

                } else {
                    if (!person.timout) {
                        console.log('Setting timeout');
                        person.timout = setTimeout(() => {
                            person.isHome = false;
                            for (const chat of chats) {
                                bot.telegram.sendMessage(chat, person.name + ` left home ${pingTimeoutInMinutes} minutes ago`);
                            }
                            console.log(person.name + ` left home ${pingTimeoutInMinutes} minutes ago`);
                        }, pingTimeout);
                    }
                }
                person.ping = result.alive;
            })
            .catch(err => console.error(err));
    }
}, pingInterval);

bot.launch().then(() => {
    console.log('Telegram bot has been started');
    process.on('SIGINT', function () {
        console.log('Stopping telegram bot');
        bot.stop(() => {
            console.log("Telegram bot has been stopped");
        });
    });
});
